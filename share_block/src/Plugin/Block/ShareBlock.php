<?php

namespace Drupal\share_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'ShareBlock' block.
 *
 * @Block(
 *  id = "share_block",
 *  admin_label = @Translation("Share block"),
 *  title = @Translation("Share block"),
 *  url = "http://www.facebook.com",
 *  image = "facebook.png"
 * )
 */
class ShareBlock extends BlockBase {


  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];

    // Get plugin definition.
    $definition = $this->getPluginDefinition();
    $module_path = drupal_get_path('module', 'share_block');
    $image = $module_path . '/files/images/' . $definition['image'];
    $url = $definition['url'];
    $title = $definition['title'];

    $build['share_block']['#markup'] = '<a href = "' . $url . '"><img src = "' . $image . '" alt="' . $title .'"></a>';

    return $build;
  }

}
